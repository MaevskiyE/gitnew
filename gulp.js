var gulp = require("gulp");
	browserSync = require('browser-sync').create(); // Подключаем Browser Sync
	sass = require('gulp-sass');
	compass = require('compass-importer');
	autoprefixer = require('gulp-autoprefixer');


gulp.task('sass', function()
{
    return gulp.src('app/scss/*.scss')
      .pipe(sass({ importer: compass }).on('error', sass.logError))
      .pipe(autoprefixer({
      	browsers: ['last 15 versions', '>1%', 'ie 8', 'ie 7'],
      	cascade: false
      }))
      .pipe(gulp.dest('./app/css'))
      .pipe(browserSync.stream())

});

gulp.task('watch', ['sass'], function () {

	browserSync.init({ // Выполняем browserSync
		server: "app"
	});

	gulp.watch('./app/scss/*.scss', ['sass']); // Наблюдение за sass файлами в папке sass
	gulp.watch('./app/*.html').on('change', browserSync.reload); // Наблюдение за HTML файлами в корне проекта
	gulp.watch('./app/сss/media.css', browserSync.reload); // Наблюдение за media файлами в папке css
	gulp.watch('./app/js/*.js', browserSync.reload); // Наблюдение за JS файлами в папке js
});